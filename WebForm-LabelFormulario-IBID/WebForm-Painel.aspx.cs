﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForm_LabelFormulario_IBID
{
    public partial class WebForm_Painel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["NomeCompleto"] = txtNomeC.Text;
                Session["Genero"] = ddlGenero.Text;
                Session["Celular"] = txtCelular.Text;
                Session["DataNasc"] = txtDataNascimento.Text;
                Session["Endereco"] = txtEndereco.Text;
                Session["Cep"] = txtCEP.Text;
                Session["Cidade"] = txtCidade.Text;
                Session["Estados"] = ddlEstado.Text;
                Session["Login"] = txtLogin.Text;


                panel1.Visible = true;
                panel2.Visible = false;
                panel3.Visible = false;


                //--------------------------------------

                DataTable dt = new DataTable();
                dt.Columns.Add("Genero", typeof(string));
                dt.Columns.Add("Valor", typeof(int));

                dt.Rows.Add("", 0);
                dt.Rows.Add("Feminino", 1);
                dt.Rows.Add("Masculino", 2);
                dt.Rows.Add("LGBTQIA+", 3);
                dt.Rows.Add("Prefiro não dizer", 4);

                ddlGenero.DataSource = dt;
                ddlGenero.DataTextField = "Genero";  // Define a coluna "Genero" como o texto exibido no DropDownList.
                ddlGenero.DataValueField = "Valor";  // Define a coluna "Valor" como o valor associado a cada item.
                ddlGenero.DataBind();

                //----------------------------------------

                string[] estados = new string[]
            {
            "", "Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal",
            "Espírito Santo", "Goiás", "Maranhão", "Mato Grosso", "Mato Grosso do Sul",
            "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro",
            "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia", "Roraima", "Santa Catarina",
            "São Paulo", "Sergipe", "Tocantins"
            };

                ddlEstado.DataSource = estados;
                ddlEstado.DataBind();
            }
        }
        protected void BotaoProximo1_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
        }

        protected void BotaoVoltar1_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            panel2.Visible = false;
            panel3.Visible = false;
        }

        protected void BotaoProximo2_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = true;
        }

        protected void BotaoVoltar2_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            panel2.Visible = true;
            panel3.Visible = false;
        }

        protected void Enviar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLogin.Text) || string.IsNullOrEmpty(txtSenha.Text))
            {
                lblAviso.Text = "Por favor, adicione informações no formulário!";
                lblAviso.Visible = true;
            }
            else
            {
                lblAviso.Text = "Dados enviados com sucesso!";
                lblAviso.Visible = true;
            }
            

            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = true;
        }

    }
}