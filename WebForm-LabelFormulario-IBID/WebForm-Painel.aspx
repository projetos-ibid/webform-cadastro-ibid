﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm-Painel.aspx.cs" Inherits="WebForm_LabelFormulario_IBID.WebForm_Painel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <link type="text/css" rel="stylesheet" href="Assets/css/formStyle.css" />
    <title>Formulário</title>
</head>
<body>
    <form id="form1" runat="server" class="form-principal">
    <table ID="tblPanelmain"style="width: 100%;">
        <asp:Panel ID="panelPrincipal" runat="server">
            
            <asp:Panel ID="panel1" runat="server">
                <h2>Informações pessoais</h2>
                  <table ID="tblPanel1"style="width: 100%;">
                    <tr>
                        <td class="titulo-linha">Nome completo:</td>
                        <td><asp:TextBox ID="txtNomeC" class="textbox-layout" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="titulo-linha">Gênero:</td>
                        <td><asp:DropDownList ID="ddlGenero" class="textbox-layout dropdown-layout" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="titulo-linha">Data de nascimento:</td>
                        <td><asp:TextBox ID="txtDataNascimento" class="textbox-layout" runat="server" TextMode="DateTime" Placeholder="DD/MM/AAAA"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="titulo-linha">Número de celular:</td>
                        <td><asp:TextBox ID="txtCelular" runat="server" class="textbox-layout" TextMode="Number" Placeholder="DDD + Número"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><asp:Button ID="btnProximo" runat="server" Text="Avançar" class="botao-avancar" OnClick="BotaoProximo1_Click"/></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="panel2" runat="server">
                <h2>Detalhes do endereço</h2>
                <table ID="tblPanel2"style="width: 100%;">
                    <tr>
                        <td class="titulo-linha">Endereço completo:</td>
                        <td><asp:TextBox ID="txtEndereco" class="textbox-layout" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="titulo-linha">Cidade:</td>
                        <td><asp:TextBox ID="txtCidade" class="textbox-layout" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="titulo-linha">Estado:</td>
                        <td><asp:DropDownList ID="ddlEstado" class="textbox-layout dropdown-layout" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="titulo-linha">CEP:</td>
                        <td><asp:TextBox ID="txtCEP" runat="server" class="textbox-layout" TextMode="Number"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="btnVoltar" runat="server" Text="Voltar" class="botao-voltar" OnClick="BotaoVoltar1_Click"/></td>
                        <td><asp:Button ID="btnProximo2" runat="server" Text="Avançar" class="botao-avancar" OnClick="BotaoProximo2_Click"/></td>
                    </tr>
                </table>
            </asp:Panel>

            <asp:Panel ID="panel3" runat="server">
                <h2>Login</h2>
                 <table ID="tblPanel3"style="width: 100%;">
                    <tr>
                        <td class="titulo-linha">Usuário:</td>
                        <td><asp:TextBox ID="txtLogin" class="textbox-layout" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="titulo-linha">Senha:</td>
                        <td><asp:TextBox ID="txtSenha" class="textbox-layout" runat="server" TextMode="Password"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td><asp:Button ID="btnVoltar2" runat="server" Text="Voltar" class="botao-voltar" OnClick="BotaoVoltar2_Click"/></td>
                        <td><asp:Button ID="btnEnviar" runat="server" Text="Enviar" class="botao-enviar" OnCLick="Enviar_Click"/></td>
                    </tr>
                 </table>
                <asp:Label ID="lblAviso" runat="server" Text="" class="label-aviso"></asp:Label>
            </asp:Panel>

            </asp:Panel>
            
    </table>
        </form> 
</body>
</html>
